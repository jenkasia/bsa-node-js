# Done task 3 from js bsa


#### How to start the code

1. `git clone https://jenkasia@bitbucket.org/jenkasia/bsa-node-js.git`
2. `cd bsa-node-js`
3. `npm i`
4. `npm start`
5. By default server running on [localhost:3000](http://localhost:3000)


### Tasks that i need to do

Write a web server using **Node.js** + **Express.js** that can handle the list of following requests:

- **GET**: _/user_  
  get an array of all users

- **GET**: _/user/:id_  
  get one user by ID

- **POST**: _/user_  
  create user according to the data from the request body

- **PUT**: _/user/:id_  
  update user according to the data from the request body

- **DELETE**: _/user/:id_  
  delete one user by ID 

##Also you can test my app on the server by heroku
[heroku-sever](https://jenkasia-bsa-node-js.herokuapp.com/)
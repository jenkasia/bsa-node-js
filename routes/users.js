var express = require('express');
var router = express.Router();

const data = [
  {
    "id": 1,
    "name": "Ryu",
    "health": 45, 
    "attack": 4, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
  },
  {
    "id": 2,
    "name": "Dhalsim",
    "health": 60, 
    "attack": 3, 
    "defense": 1,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
  },
  {
    "id": 3,
    "name": "Guile",
    "health": 45, 
    "attack": 4, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/guile-hdstance.gif"
  },
  {
    "id": 4,
    "name": "Zangief",
    "health": 60, 
    "attack": 4, 
    "defense": 1,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/zangief-hdstance.gif"
  },
  {
    "id": 5,
    "name": "Ken",
    "health": 45, 
    "attack": 3, 
    "defense": 4,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif"
  },
  {
    "id": 6,
    "name": "Bison",
    "health": 45, 
    "attack": 5, 
    "defense": 4,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
  },
  {
    "id": 7,
    "name": "Chun-Li",
    "health": 40, 
    "attack": 3, 
    "defense": 8,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/chunli-hdstance.gif"
  },
  {
    "id": 8,
    "name": "Blanka",
    "health": 80, 
    "attack": 8, 
    "defense": 2,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/blanka-hdstance.gif"
  },
  {
    "id": 9,
    "name": "E.Honda",
    "health": 50, 
    "attack": 5, 
    "defense": 5,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ehonda-hdstance.gif"
  },
  {
    "id": 10,
    "name": "Balrog",
    "health": 55, 
    "attack": 4, 
    "defense": 6,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/balrog-hdstance.gif"
  },
  {
    "id": 11,
    "name": "Vega",
    "health": 50, 
    "attack": 4, 
    "defense": 7,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/vega-hdstance.gif"
  },
  {
    "id": 12,
    "name": "Sagat",
    "health": 55, 
    "attack": 4, 
    "defense": 6,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/sagat-hdstance.gif"
  },
  {
    "id": 13,
    "name": "Cammy",
    "health": 45, 
    "attack": 4, 
    "defense": 7,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/cammy-hdstance.gif"
  },
  {
    "id": 14,
    "name": "Fei Long",
    "health": 80, 
    "attack": 2, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/feilong-hdstance.gif"
  },
  {
    "id": 15,
    "name": "Dee Jay",
    "health": 50, 
    "attack": 5, 
    "defense": 5,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/deejay-hdstance.gif"
  },
  {
    "id": 16,
    "name": "T.Hawk",
    "health": 60, 
    "attack": 5, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/thawk-hdstance.gif"
  },
  {
    "id": 17,
    "name": "Akuma",
    "health": 70, 
    "attack": 5, 
    "defense": 5,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/akuma-hdstance.gif"
  }
];


const {
  saveName
} = require("../services/user.service");
const {
  isAuthorized
} = require("../middlewares/auth.middleware");

router.get('/', (req, res, next) => {
  res.statusCode = 200;
  res.send(data)
  // //console.log(data)
})

router.get('/:id', (req, res, next) => {

  if (!(req.params.id > 0 && req.params.id <= data.length)) {
    res.statusCode = 400;
    res.end('This data does not exist')
    return;
  }
  const returnResult = data.find((item) => item.id === parseInt(req.params.id));
  res.statusCode = 200;
  res.send(returnResult)
});


router.get('/:id', (req, res, next) => {
  //console.log(req.params.id)
  // //console.log(typeof(parseFloat(req.params.id)))
  if (!(req.params.id > 0 && req.params.id <= data.length)) {
    res.statusCode = 400;
    res.end('This data does not exist')
    return;
  }
  const returnResult = data.find((item) => {
    //console.log(req.params.id)
    item.id === parseInt(req.params.id)
  });
  //console.log(returnResult)
  res.statusCode = 200;
  res.send(returnResult)
});


router.post('/', (req, res, next) => {

  if (!req.body || !req.body.name || !req.body.health || !req.body.attack || !req.body.defense || !req.body.source) {
    // //console.log(req.body,req.body.first_name,req.body.last_name,req.body.email)
    res.statusCode = 400;
    res.end('Dont get data');
    return;
  }
  //console.log(req.body, req.body.name, req.body.health, req.body.attack, req.body.defense, req.body.source)
  req.body.id = data.length + 1
  data.push(req.body)
  res.send(req.body);
});

router.put('/:id', (req, res, next) => {
  if (!req.body || !req.body.name || !req.body.health || !req.body.attack || !req.body.defense || !req.body.source) {
    res.statusCode = 400;
    res.end('Dont get data');
    return;
  }
  const idOfDataItem = data.findIndex(item => {
    //console.log(item.id, req.params.id)
    return item.id == parseInt(req.params.id)
  })
  data[idOfDataItem].name = req.body.name
  data[idOfDataItem].health = req.body.health
  data[idOfDataItem].attack = req.body.attack
  data[idOfDataItem].defense = req.body.defense
  data[idOfDataItem].source = req.body.source
  res.send(req.body);
});

router.delete('/:id', (req, res, next) => {
  const idOfDataItem = data.findIndex(item => {
    //console.log(item.id, req.params.id)
    return item.id == parseInt(req.params.id)
  })
  //console.log(idOfDataItem)
  if (idOfDataItem === -1) {
    res.statusCode = 400;
    res.end('This data does not exist')
  }
  else{
  data.splice(idOfDataItem, 1);
  res.send(data);
  }
});

module.exports = router;